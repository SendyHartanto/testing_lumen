<?php

namespace App\Http\Controllers;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function index(){
        $data = User::get();

        return response($data);
    }

    public function detail($id){
        $data = getUser($id);

        return $data;
    }

    public function store(Request $req){
        return $req;
    }
}
