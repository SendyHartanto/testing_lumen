<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Closure;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    public function checkAuth($email,$password){    
        return file_get_contents('http://maillogin.ved.carsworld.id/?username='.$email.'&password='.$password);
    }

    public function login(Request $request){
        $user = User::where('email',$request->email)->first(); 
        $check = $this->checkAuth($request->email,$request->password);

        if (json_decode($check)->login == "success") {
            if($user){
                $jwt = JWT::encode($request->only('email','password'),env('JWT_SECRET'));
    
                $response = [
                    'result' => 'success',
                    'code' => '200',
                    'token' => $jwt,
                    'user_data' => $user,
                ];
    
                return response($response);
            }else{
                $response = [
                    'result' => 'failed (Unauthorized)',
                    'message' => 'Email anda tidak terdaftar di database kami',
                    'code' => '401'
                ];
                
                return response($response);
            }
        }else{
            $response = [
                'result' => 'failed (Unauthorized)',
                'message' => 'Email anda tidak terdaftar di database carsworld',
                'code' => '401'
            ];
            
            return response($response);
        }

        $response = [
            'result' => 'failed (Unauthorized)',
            'code' => '401'
        ];

        return response($response);
    }

}
